# The LFP Pipeline

The LFP Pipeline provides a framework for projects to build a GitLab
Pipeline for their project.

## Contents

* [Getting Started](source/getting-started.md)
* [Tools](https://gitlab.com/LibreFoodPantry/common-services/tools) that we
have written to be added to pipelines. Tools (except linters) are all
*enabled by default* in the pipeline. You can disable any of them in your
project's `.gitlab-ci.yml` file. See
[Configuration](https://gitlab.com/LibreFoodPantry/common-services/tools/documentation/-/blob/main/source/getting-started.md#configuring).
* [Linters](https://gitlab.com/LibreFoodPantry/common-services/tools/linters)
that we have implemented to be added to pipelines. Linters are all
*disabled by default* in the pipeline. You can enable any of them in your
project's `.gitlab-ci.yml` file. See the README in each linter's
project for details on adding it to your project's `.gitlab-ci.yml` and
how to configure the linter.
  * There are multiple sources available for
    [third-party pipeline components](source/pipeline-components.md).
    We have added some of them to the LFP Pipeline. You can add others to
    your own project.
  * [Epic for linters to be added/considered](https://gitlab.com/groups/LibreFoodPantry/common-services/tools/linters/-/epics/2)
* [Multi-Platform Images](source/multi-platform.md)
* [Preparing for next release](source/next-release.md)
