# Building Multi-Platform Images in the Pipeline

## `.gitlab-ci.yml`

Use the PIPELINE_IMAGE_PLATFORMS variable in .gitlab-ci.yml to specify
the platforms for which you want images built. This enables the new
multi-platform build feature. If this variable is undefined or empty,
the original pipeline behavior is used. The value of this variable is
passed to the `--platform` option of the `docker buildx build` command.
See [https://docs.docker.com/build/building/multi-platform/](https://docs.docker.com/build/building/multi-platform/)
for its syntax.

For example, this .gitlab-ci requests images for two platforms be built.

```yaml
include:
  - remote: https://gitlab.com/LibreFoodPantry/common-services/tools/pipeline/-/raw/main/source/gitlab-ci/pipeline.yml

variables:
  PIPELINE_IMAGE_PLATFORMS: "linux/amd64,linux/arm64"
```

## `./commands/build.sh`

If the `PIPELINE_IMAGE_PLATFORMS` set to a non-empty value, the pipeline
defines `PIPELINE_BUILDX_BUILD_OPTIONS` and `PIPELINE_IMAGE_NAME` variables
and calls the project's build command. The build command should use these
variables to detect if it's being called in the pipeline or locally, and then
build the image(s) using the appropriate command.

`PIPELINE_BUILDX_BUILD_OPTIONS` is populated with the appropriate options
to pass to `docker buildx build`. If it is non-empty, the build script should
call `docker buildx build` and pass it `$PIPELINE_BUILDX_BUILD_OPTIONS`
unquoted.

`PIPELINE_IMAGE_NAME` is populated with the name that the image should be
tagged with. If it is non-empty, then the pipeline is calling the build script
to produce a single image in the current environment.

If neither `PIPELINE_BUILDX_BUILD_OPTIONS` or `PIPELINE_IMAGE_NAME` are set,
then the build command was called locally by the developer, and it should
build an image appropriate for the local environment.

Here is an example `commands/build.sh` that embodies the above.

```bash
#!/usr/bin/env bash
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}/.."

DOCKERFILE="./gitkit-deploy.dockerfile"
BUILD_CONTEXT="./"

# Name to use for local builds.
LOCAL_IMAGE_NAME="my-cool-project:dev"

if [[ -n "$PIPELINE_BUILDX_BUILD_OPTIONS" ]] ; then
    docker buildx build $PIPELINE_BUILDX_BUILD_OPTIONS \
        --file "$DOCKERFILE" "$BUILD_CONTEXT"
elif [[ -n "$PIPELINE_IMAGE_NAME" ]] ; then
    docker build --tag $PIPELINE_IMAGE_NAME \
        --file "$DOCKERFILE" "$BUILD_CONTEXT"
else
    docker build --tag "$LOCAL_IMAGE_NAME" \
        --file "$DOCKERFILE" "$BUILD_CONTEXT"
fi
```

It's also possible for the build command to do other work besides just
building images. For example, it could build a multi-file OpenAPI specification
into a single file. This could be done before or after building the image.

## `./commands/test.sh`

If `test.sh` is called locally, it can use the image by the same local
name as was used in the build command (see above).

If `test.sh` is called in the pipeline, then `PIPELINE_IMAGE_NAME` will
be set to the name that the test should use to refer to the image to test.

For example, suppose the image contains an internal `unittest` command
to run all the unit tests...

```bash
#!/usr/bin/env bash
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}/.."

# Name to use for local builds.
LOCAL_IMAGE_NAME="my-cool-project:dev"

if [[ -n "$PIPELINE_IMAGE_NAME" ]]; then
  docker run -it --rm --entrypoint unittest "$PIPELINE_IMAGE_NAME"
else
  docker run -it --rm --entrypoint unittest "$LOCAL_IMAGE_NAME"
fi
```
