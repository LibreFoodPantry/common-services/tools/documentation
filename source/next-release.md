# Preparing for the next release

## `IMAGE_NAME` is deprecated and will be removed

`IMAGE_NAME` is deprecated, and will be removed in the next release.
Projects set it in their `.gitlab-ci.yml` to tell the pipeline the
name they would like to use for the constructed image from within
the scripts in commands. Instead, commands should now use `PIPELINE_IMAGE_NAME`
to discover the image name that the pipeline is using to refer to the
constructed image. For example, a command might do the following to
determine the name it should use for the image.

```bash
NAME=""
if [[ -n "$PIPELINE_IMAGE_NAME" ]]; then
    NAME="$PIPELINE_IMAGE_NAME"
else
    NAME="cool-project:latest"
fi
```

`IMAGE_NAME` no longer needs to be set in `.gitlab-ci.yml` and should be removed.
Although leaving it should not hurt anything. It's just cruft.
