# Third-Party Pipeline Components

There are multiple sources available for pipeline components from third
parties. We have used some in the LFP pipeline for
[linters](https://gitlab.com/LibreFoodPantry/common-services/tools/linters).

## Pipeline Components

> A project that builds small and fast, constantly updated containers for
CI/CD purposes. In doing so we hope to help developers to focus on their
projects instead on building the tools they need for the project.

from [Pipeline Components](https://pipeline-components.dev/)

For an example see [yamllint used in the LFP pipeline](https://gitlab.com/LibreFoodPantry/common-services/tools/linters/yamllint/-/blob/main/source/gitlab-ci/yamllint.yml).

## Cardboard CI

> CardboardCI is a collection of Docker images intended to be used for
continuous integration. All of the images are pre-built and made available
through the GitHub Container Registry. These images are intended to upgrade
automatically and enforce a series of constraints to ensure consistent
behaviour.

from [Cardboard CI](https://cardboardci.jrbeverly.me/)

For an example see [stylelint used in the LFP pipeline](https://gitlab.com/LibreFoodPantry/common-services/tools/linters/stylelint/-/blob/main/source/gitlab-ci/stylelint.yml).

## R2Devops Marketplace

> Plug and play CI/CD templates. Access over 150 opensource templates and
save valuable time

from [R2Devops Marketplace](https://r2devops.io/marketplace)
